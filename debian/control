Source: python-os-faults
Section: python
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 Thomas Goirand <zigo@debian.org>,
Build-Depends:
 debhelper-compat (= 10),
 dh-python,
 openstack-pkg-tools (>= 99~),
 python3-all,
 python3-pbr,
 python3-setuptools,
 python3-sphinx,
Build-Depends-Indep:
 python3-click,
 python3-coverage,
 python3-ddt,
 python3-hacking,
 python3-iso8601,
 python3-jsonschema,
 python3-mock,
 python3-openstackdocstheme,
 python3-oslo.concurrency,
 python3-oslo.i18n,
 python3-oslo.serialization,
 python3-oslo.utils,
 python3-oslotest,
 python3-platformdirs,
 python3-pyghmi,
 python3-pytest,
 python3-pytest-cov,
 python3-six,
 python3-sphinx-rtd-theme,
 python3-sphinxcontrib.programoutput,
 python3-testscenarios,
 python3-testtools,
 python3-yaml,
 subunit,
 testrepository,
Standards-Version: 4.2.0
Vcs-Browser: https://salsa.debian.org/openstack-team/libs/python-os-faults
Vcs-Git: https://salsa.debian.org/openstack-team/libs/python-os-faults.git
Homepage: http://os-faults.readthedocs.io/

Package: python-os-faults-doc
Section: doc
Architecture: all
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Description: OpenStack fault-injection library - doc
 The library does destructive actions inside an OpenStack cloud. It provides an
 abstraction layer over different types of cloud deployments. The actions are
 implemented as drivers (e.g. DevStack driver, Fuel driver, Libvirt driver,
 IPMI driver).
 .
 This package contains the documentation.

Package: python3-os-faults
Architecture: all
Depends:
 python3-click,
 python3-iso8601,
 python3-jsonschema,
 python3-oslo.concurrency,
 python3-oslo.i18n,
 python3-oslo.serialization,
 python3-oslo.utils,
 python3-pbr,
 python3-platformdirs,
 python3-pyghmi,
 python3-six,
 python3-yaml,
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 python-os-faults-doc,
Description: OpenStack fault-injection library - Python 3.x
 The library does destructive actions inside an OpenStack cloud. It provides an
 abstraction layer over different types of cloud deployments. The actions are
 implemented as drivers (e.g. DevStack driver, Fuel driver, Libvirt driver,
 IPMI driver).
 .
 This package contains the Python 3.x module.
